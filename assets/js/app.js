import '../css/app.scss';

const $ = require('jquery');
import {init as flashMessageInit} from "./flashe";
import "./appeal/appeal";

$(document).ready(function () {
  flashMessageInit();
});
