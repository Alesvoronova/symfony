const $ = require('jquery');
require('bootstrap');
import {FlasheMessage} from "../flashe";

$(document).ready(function () {
  const flasheMessage = new FlasheMessage();
  $('#open-appeal-modal').click(function () {
    const appealModal = $('#appeal-send-modal');
    console.log(appealModal);
    appealModal.modal();
    const body = appealModal.find('.modal-body');
    body.text('loading');
    $.ajax({
      url: $(this).attr('data-url'),
      type: 'GET',
      dataType: 'html',
      success: function (data) {
        body.html(data);
      },
      error: function (xhr, status) {
        flasheMessage.error('Sorry, there was a problem!');
      },
    });
  });

  $(document).on('submit', 'form[name="appeal"]', function (e) {
    e.preventDefault();

    const form = $(this);
    const url = form.attr('action');

    $.ajax({
      type: 'POST',
      url: url,
      data: form.serialize(),
      success: function (data) {
        const appealModal = $('#appeal-send-modal');
        const body = appealModal.find('.modal-body');
        if (typeof data === 'object') {
          if (data.status === 'success') {
            $('#appeal-send-modal').modal('hide');
            body.html('');
          }
          flasheMessage.display(data);
        } else {
          body.html(data);
        }
      },
      error: function (data) {
        console.log(data);
        flasheMessage.error('Sorry, there was a problem!');
      }
    });
  });
});
