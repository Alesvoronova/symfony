import toastr from 'toastr';
import $ from 'jquery';

export function init () {
  $('.flash-message').each(function () {
    const $this = $(this);
    show($this.data('type'), $this.text());
  });
}

export class FlasheMessage {
  display (data) {
    if (typeof data.status !== 'undefined') {
      show(data.status, data.message)
    }
  }
  error (message) {
    toastr.error(message);
  }
}

const show = function (type, message) {
  if (typeof message === 'undefined') {
    return;
  }
  switch (type) {
    case 'success':
      toastr.success(message);
      break;
    case 'error':
      toastr.error(message);
      break;
    default:
      toastr.error(message);
  }
};
