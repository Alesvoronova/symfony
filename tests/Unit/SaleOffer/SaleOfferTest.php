<?php

namespace App\Tests\Unit\SaleOffer;

use App\Model\Product\Entity\Product;
use App\Model\SaleOffer\Entity\SaleOffer;
use App\Model\User\Entity\Counterparty;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class SaleOfferTest extends TestCase
{
    public function testCreate(): void
    {
        /** @var $product */
        $product = $this->createMock(Product::class);
        $counterparty = $this->createMock(Counterparty::class);

        $faker = Factory::create();
        $id = $faker->uuid;
        $createdAt = new \DateTimeImmutable();
        $comment = $faker->text;
        $price = $faker->numberBetween(0, 1000);
        $currency = 2;
        $count = 1;
        $saleOffer = new SaleOffer(
            $id,
            $product,
            $counterparty,
            $createdAt,
            $comment,
            $price,
            $currency,
            $count
        );

        self::assertInstanceOf(SaleOffer::class, $saleOffer);
        self::assertEquals($id, $saleOffer->getId());
        self::assertEquals($price, $saleOffer->getPrice());
        self::assertEquals($createdAt, $saleOffer->getCreatedAt());
        self::assertEquals($counterparty, $saleOffer->getCounterparty());
        self::assertEquals($product, $saleOffer->getProduct());
        self::assertEquals($comment, $saleOffer->getComment());
    }
}
