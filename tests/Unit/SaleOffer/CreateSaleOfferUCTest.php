<?php

declare(strict_types=1);

namespace App\Tests\Unit\SaleOffer;

use App\Model\Product\Entity\Product;
use App\Model\Product\Repository\ProductRepository;
use App\Model\SaleOffer\Entity\SaleOffer;
use App\Model\SaleOffer\UseCase\CreateSaleOfferDto;
use App\Model\SaleOffer\UseCase\CreateSaleOfferUC;
use App\Model\User\Entity\Counterparty;
use App\Model\User\Repository\CounterpartyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class CreateSaleOfferUCTest extends TestCase
{
    public function testCreate(): void
    {
        /** @var EntityManagerInterface $em */
        $em = $this->createMock(EntityManagerInterface::class);
        $productRepository = $this->createMock(ProductRepository::class);
        $counterpartyRepository = $this->createMock(CounterpartyRepository::class);

        $faker = Factory::create();
        $product = $this->createMock(Product::class);
        $productId = $faker->uuid;
        $counterparty = $this->createMock(Counterparty::class);
        $counterpartyId = $faker->uuid;

        $productRepository
            ->expects(self::once())
            ->method('getById')
            ->with($productId)
            ->willReturn($product);

        $counterpartyRepository
            ->expects(self::once())
            ->method('getById')
            ->with($counterpartyId)
            ->willReturn($counterparty);

        $dto = new CreateSaleOfferDto();
        $dto->article = 1;
        $dto->product = $productId;
        $dto->counterparty = $counterpartyId;
        $dto->count = $count = $faker->numberBetween(1, 100);
        $dto->price = $price = $faker->numberBetween(1, 100);
        $dto->comment = $comment = $faker->text;
        $dto->currency = 1;

        $createSaleOfferUC = new CreateSaleOfferUC(
            $em,
            $productRepository,
            $counterpartyRepository
        );

        $saleOffer = $createSaleOfferUC->handle($dto);
        self::assertInstanceOf(SaleOffer::class, $saleOffer);
        self::assertEquals($saleOffer->getProduct(), $product);
        self::assertEquals($saleOffer->getCounterparty(), $counterparty);
        self::assertEquals($saleOffer->getPrice(), $price);
        self::assertEquals($saleOffer->getCount(), $count);
        self::assertEquals($saleOffer->getComment(), $comment);
    }
}
