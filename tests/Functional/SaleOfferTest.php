<?php

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SaleOfferTest extends WebTestCase
{
    public function testGuest(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertSame(302, $client->getResponse()->getStatusCode());
        //in headers exist Location with that address
        $this->assertSame('http://localhost/login', $client->getResponse()->headers->get('Location'));
    }

    public function crawler(): void
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@mail.com',
        ]);
        $crawler = $client->request('GET', '/');
        $crawler->filter('h1')->text();
    }
}
