#!/bin/sh

PASS=true

docker -v
if [ $? -eq 0 ]; then #if install docker, use it
  echo "\nValidating PHPCS:\n"
  docker-compose run --rm stock-php-fpm php ./vendor/bin/phpcs -s

  if [ $? -eq 0 ]; then
    echo "\033[32mPHPCS Passed! \033[0m"
  else
    echo "\033[41mPHPCS Failed! \033[0m"
    PASS=false
    exit 1;
  fi

  echo "\nValidating PHPStan:\n"
  docker-compose run --rm stock-php-fpm composer phpstan
  if [ $? -eq 0 ]; then
    echo "\033[32mPHPStan Passed! \033[0m"
  else
    echo "\033[41mPHPStan Failed! \033[0m"
    PASS=false
  fi
else
  which ./vendor/bin/phpcs &> /dev/null
  if [ $? -eq 1 ]; then
    echo "\033[41mPlease install PHPCS\033[0m"
    exit 1
  fi

  php ./vendor/bin/phpcs -s
  if [ $? -eq 0 ]; then
    echo "\033[32mPHPCS Passed! \033[0m"
  else
    echo "\033[41mPHPCS Failed! \033[0m"
    PASS=false
    exit 1;
  fi
fi

if ! $PASS; then
  echo "\033[41mCOMMIT FAILED:\033[0m Your commit contains files that should pass PHPCS but do not. \n"
  echo "\033[41mCOMMIT FAILED:\033[0m Please fix the PHPCS errors and try again.\n"
  exit 1
  # ВРЕМЕННО отключен phpcs при коммите до исправления форматирвоания кода
  # exit 0
else
  echo "\033[42mCOMMIT SUCCEEDED\033[0m\n"
fi

exit $?
