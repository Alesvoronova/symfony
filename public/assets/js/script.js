$(document).ready(function () {
    $('#open-appeal-modal').click(function () {
        var appealModal = $('#appeal-send-modal');
        appealModal.modal();
        var body = appealModal.find('.modal-body');
        body.text('loading');
        $.ajax({
            url: $(this).attr('data-url'),
            type: 'GET',
            dataType: 'html',
            success: function (data) {
                body.html(data);
            },
            error: function (xhr, status) {
                alert('Sorry, there was a problem!');
            },
        });
    });

    $(document).on('submit', 'form[name="appeal"]', function (e) {
        e.preventDefault();

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: 'POST',
            url: url,
            data: form.serialize(),
            success: function (data) {
                var appealModal = $('#appeal-send-modal');
                var body = appealModal.find('.modal-body');
                if (typeof data === 'object') {
                    if (data.status === 'success') {
                        $('#appeal-send-modal').modal('hide');
                        body.html('');
                    }
                    flashMessage(data);
                } else {
                    body.html(data);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    });


  $('.open-user-info').click(function (e) {
    e.preventDefault();
    var userModal = $('#user-info');
    userModal.modal();
    var body = userModal.find('.modal-body');
    body.text('loading');
    var url = $(this).attr('href');
    console.log(url);
    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'html',
      success: function (data) {
        body.html(data);
      },
      error: function (xhr, status) {
        alert('Sorry, there was a problem!');
      },
    });
  });
});


    $(document).on('submit', 'form[name="admin_appeal"]', function (e) {
      e.preventDefault();

      var form = $(this);
      var url = form.attr('action');

      $.ajax({
        type: 'POST',
        url: url,
        data: form.serialize(),
        success: function (data) {
          var userModal = $('#user-info');
          var body = userModal.find('.modal-body');
          if (typeof data === 'object') {
            if (data.status === 'success') {
              $('#user-info').modal('hide');
              body.html('');
            }
            flashMessage(data);
          } else {
            body.html(data);
          }
        },
        error: function (data) {
          console.log(data);
        }
      });
    });

function flashMessage(data) {
    if (typeof data.status !== 'undefined') {
        if (data.status === 'success') {
            //success message
            alert(data.message);
        } else {
            //error message
            alert(data.message);
        }
    }
}
