<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200907143823 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE connection_request ADD created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE connection_request ADD type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE counterparties ADD email VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE connection_request DROP created_at');
        $this->addSql('ALTER TABLE connection_request DROP type');
        $this->addSql('ALTER TABLE "counterparties" DROP email');
    }
}
