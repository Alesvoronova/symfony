<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200904145411 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE appleas (id UUID NOT NULL, 
                        fio VARCHAR(255) NOT NULL, 
                        email VARCHAR(255) NOT NULL, 
                        subject VARCHAR(255) NOT NULL, 
                        description TEXT NOT NULL, 
                        created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
                        is_read TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE appleas');
    }
}
