<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200908134151 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE appleas ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE appleas ADD answer TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE appleas DROP is_read');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE appleas DROP updated_at');
        $this->addSql('ALTER TABLE appleas DROP answer');
        $this->addSql('ALTER TABLE appleas ADD is_read TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }
}
