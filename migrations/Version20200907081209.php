<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200907081209 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "cities_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "countries_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_units_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "cities" (id INT NOT NULL, country_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D95DB16BF92F3E70 ON "cities" (country_id)');
        $this->addSql('CREATE TABLE connection_request (id UUID NOT NULL, tariff_id UUID DEFAULT NULL, counterparty_id UUID DEFAULT NULL, is_approve BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_409D69BC92348FD2 ON connection_request (tariff_id)');
        $this->addSql('CREATE INDEX IDX_409D69BCDB1FAD05 ON connection_request (counterparty_id)');
        $this->addSql('COMMENT ON COLUMN connection_request.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN connection_request.tariff_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN connection_request.counterparty_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "counterparties" (id UUID NOT NULL, city_id INT DEFAULT NULL, country_id INT DEFAULT NULL, company_name VARCHAR(255) NOT NULL, ynp INT NOT NULL, kpp INT DEFAULT NULL, zip_code INT NOT NULL, phone VARCHAR(255) DEFAULT NULL, contact_first_name VARCHAR(255) NOT NULL, contact_middle_name VARCHAR(255) DEFAULT NULL, contact_last_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6FE3271B8BAC62AF ON "counterparties" (city_id)');
        $this->addSql('CREATE INDEX IDX_6FE3271BF92F3E70 ON "counterparties" (country_id)');
        $this->addSql('CREATE UNIQUE INDEX company_unique ON "counterparties" (ynp, country_id)');
        $this->addSql('COMMENT ON COLUMN "counterparties".id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE "countries" (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE sale_offers (id UUID NOT NULL, product_id UUID NOT NULL, counterparty_id UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, comment TEXT NOT NULL, price INT DEFAULT NULL, count INT NOT NULL, views INT NOT NULL, expires TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, currency INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AE270AE54584665A ON sale_offers (product_id)');
        $this->addSql('CREATE INDEX IDX_AE270AE5DB1FAD05 ON sale_offers (counterparty_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AE270AE5DB1FAD054584665A ON sale_offers (counterparty_id, product_id)');
        $this->addSql('COMMENT ON COLUMN sale_offers.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN sale_offers.product_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN sale_offers.counterparty_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN sale_offers.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN sale_offers.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN sale_offers.expires IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "tariffs" (id UUID NOT NULL, name VARCHAR(255) NOT NULL, cost DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN "tariffs".id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE "cities" ADD CONSTRAINT FK_D95DB16BF92F3E70 FOREIGN KEY (country_id) REFERENCES "countries" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE connection_request ADD CONSTRAINT FK_409D69BC92348FD2 FOREIGN KEY (tariff_id) REFERENCES "tariffs" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE connection_request ADD CONSTRAINT FK_409D69BCDB1FAD05 FOREIGN KEY (counterparty_id) REFERENCES "counterparties" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "counterparties" ADD CONSTRAINT FK_6FE3271B8BAC62AF FOREIGN KEY (city_id) REFERENCES "cities" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "counterparties" ADD CONSTRAINT FK_6FE3271BF92F3E70 FOREIGN KEY (country_id) REFERENCES "countries" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sale_offers ADD CONSTRAINT FK_AE270AE54584665A FOREIGN KEY (product_id) REFERENCES products (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sale_offers ADD CONSTRAINT FK_AE270AE5DB1FAD05 FOREIGN KEY (counterparty_id) REFERENCES "counterparties" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP INDEX uniq_b3ba5a5af8bd700d');
        $this->addSql('ALTER TABLE products ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE products ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN products.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE INDEX IDX_B3BA5A5AF8BD700D ON products (unit_id)');
        $this->addSql('ALTER TABLE users ADD counterparty_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE users ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE users ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN users.counterparty_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN users.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9DB1FAD05 FOREIGN KEY (counterparty_id) REFERENCES "counterparties" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1483A5E9DB1FAD05 ON users (counterparty_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "counterparties" DROP CONSTRAINT FK_6FE3271B8BAC62AF');
        $this->addSql('ALTER TABLE connection_request DROP CONSTRAINT FK_409D69BCDB1FAD05');
        $this->addSql('ALTER TABLE sale_offers DROP CONSTRAINT FK_AE270AE5DB1FAD05');
        $this->addSql('ALTER TABLE "users" DROP CONSTRAINT FK_1483A5E9DB1FAD05');
        $this->addSql('ALTER TABLE "cities" DROP CONSTRAINT FK_D95DB16BF92F3E70');
        $this->addSql('ALTER TABLE "counterparties" DROP CONSTRAINT FK_6FE3271BF92F3E70');
        $this->addSql('ALTER TABLE connection_request DROP CONSTRAINT FK_409D69BC92348FD2');
        $this->addSql('DROP SEQUENCE "cities_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "countries_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE product_units_id_seq CASCADE');
        $this->addSql('DROP TABLE "cities"');
        $this->addSql('DROP TABLE connection_request');
        $this->addSql('DROP TABLE "counterparties"');
        $this->addSql('DROP TABLE "countries"');
        $this->addSql('DROP TABLE sale_offers');
        $this->addSql('DROP TABLE "tariffs"');
        $this->addSql('DROP INDEX IDX_1483A5E9DB1FAD05');
        $this->addSql('ALTER TABLE "users" DROP counterparty_id');
        $this->addSql('ALTER TABLE "users" ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE "users" ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN "users".id IS NULL');
        $this->addSql('DROP INDEX IDX_B3BA5A5AF8BD700D');
        $this->addSql('ALTER TABLE products ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE products ALTER id DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN products.id IS NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_b3ba5a5af8bd700d ON products (unit_id)');
    }
}
