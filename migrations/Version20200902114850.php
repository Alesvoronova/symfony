<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200902114850 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add Product and Units tables';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE product_units (id INT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, symbol VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE products (id UUID NOT NULL, unit_id INT DEFAULT NULL, barcode VARCHAR(255) NOT NULL, name TEXT DEFAULT NULL, description TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B3BA5A5AF8BD700D ON products (unit_id)');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5AF8BD700D FOREIGN KEY (unit_id) REFERENCES product_units (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE products DROP CONSTRAINT FK_B3BA5A5AF8BD700D');
        $this->addSql('DROP TABLE product_units');
        $this->addSql('DROP TABLE products');
    }
}
