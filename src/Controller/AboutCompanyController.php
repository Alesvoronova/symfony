<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutCompanyController extends AbstractController
{
    /**
     * @Route("/company/about", name="about_company")
     */
    public function about(): Response
    {
        return $this->render('about_company/about.html.twig', [
            'isActiveMenu1' => true,
            'title' => 'wdwd',
        ]);
    }

    /**
     * @Route("/company/policy", name="privacy_policy")
     */
    public function policy(): Response
    {
        return $this->render('about_company/policy.html.twig', [
            'isActiveMenu2' => true,
        ]);
    }

    /**
     * @Route("/company/term-of-use", name="term_of_use")
     */
    public function termOfUse(): Response
    {
        return $this->render('about_company/term_of_use.html.twig', [
            'isActiveMenu3' => true,
        ]);
    }

    /**
     * @Route("/company/publish-rules", name="publish_rules")
     */
    public function publishRules(): Response
    {
        return $this->render('about_company/publish_rules.html.twig', [
            'isActiveMenu4' => true,
        ]);
    }

    /**
     * @Route("/company/tariffs", name="tariffs")
     */
    public function tariffs(): Response
    {
        return $this->render('about_company/tariffs.html.twig', [
            'isActiveMenu5' => true,
        ]);
    }
}
