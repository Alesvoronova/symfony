<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Form\Admin\AdminAppealType;
use App\Model\Appeal\Entity\Appeal;
use App\Model\Appeal\UseCase\Answer\AnswerUC;
use App\Model\Appeal\UseCase\Answer\AppealAnswerDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminAppealController extends AbstractController
{
    /**
     * @Route("/admin/appeal/{id}/answer", name="appeal_answer", methods={"GET","POST"})
     */
    public function getAnswerForm(Appeal $appeal, Request $request, AnswerUC $uc)
    {
        $dto = new AppealAnswerDto();
        $form = $this->createForm(AdminAppealType::class, $dto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $dto->appeal = $appeal;
                $uc->handle($dto);

                return $this->json([
                    'status' => 'success',
                    'message' => 'Ответ отправлен'
                ]);
            } catch (\Exception $exception) {
                return $this->json([
                    'status' => 'error',
                    'message' => 'Ошибка при отправке ответа'
                ]);
            }
        }

        return $this->render('admin/appeal/_form.html.twig', [
            'appeal' => $appeal,
            'form' => $form->createView(),
        ]);
    }
}
