<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Model\User\Entity\ConnectionRequest;
use App\Model\User\Repository\ConnectionRequestRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdminConnectionRequestController extends AbstractController
{
    /**
     * @Route("/connections", name="connection_requests_index")
     */
    public function index(ConnectionRequestRepository $repository): Response
    {
        $connectionRequests = $repository->findAllWithSorting();

        return $this->render('admin/connection_request/index.html.twig', [
            'connections' => $connectionRequests,
        ]);
    }

    /**
     * @Route("/connection/view/{id}", name="connection_requests_view", methods={"GET"})
     */
    public function view(ConnectionRequest $connectionRequest): Response
    {
        return $this->render('admin/connection_request/view.html.twig', [
            'connection_request' => $connectionRequest,
        ]);
    }

    /**
     * @Route("/connection/approve/{id}", name="connection_requests_approve", methods={"GET"})
     */
    public function approve(ConnectionRequest $connectionRequest, UserRepository $repository): Response
    {
        $counterpartyUsers = $repository->findAllUsersByCounterparty($connectionRequest->getCounterparty());

        foreach ($counterpartyUsers as $user) {
            $user->approve();
        }

        $connectionRequest->approve();
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('connection_requests_index');
    }
}
