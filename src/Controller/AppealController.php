<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\AppealType;
use App\Model\Appeal\Send\AppealCommand;
use App\Model\Appeal\Send\SendUseCaseHandler;
use App\Repository\AppealRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/appeal")
 */
class AppealController extends AbstractController
{
    /**
     * @Route("/", name="appeal_index", methods={"GET"})
     */
    public function index(AppealRepository $appealRepository): Response
    {
        return $this->render('admin/appeal/index.html.twig', [
            'appeals' => $appealRepository->getAllUnhandles(),
        ]);
    }

    /**
     * @Route("/new", name="appeal_new", methods={"GET","POST"})
     */
    public function send(Request $request, SendUseCaseHandler $useCase): Response
    {
        $cmd = new AppealCommand();
        $form = $this->createForm(AppealType::class, $cmd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $useCase->handle($cmd);
                return $this->json([
                    'status' => 'success',
                    'message' => 'Учетная запись создана'
                ]);
            } catch (\Exception $exception) {
                return $this->json([
                    'status' => 'error',
                    'message' => 'Ошибка при создании учетной записи'
                ]);
            }
        }

        return $this->render('appeal/_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
