<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ConnectionRequestType;
use App\Model\User\UseCase\ConnectionRequestCommand;
use App\Model\User\UseCase\ConnectionRequestHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("profile/")
 */
class ConnectionRequestController extends AbstractController
{
    private ConnectionRequestHandler $handler;

    public function __construct(ConnectionRequestHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @Route("connection/new", name="connection_request_new")
     */
    public function new(Request $request, ConnectionRequestCommand $command)
    {
        $form = $this->createForm(ConnectionRequestType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->handler->handle($command, $this->getUser());
            $this->addFlash('success', 'Вы успешно оставили заявку на подключение');

            return $this->redirectToRoute('home');
        }

        return $this->render('connection_request/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
