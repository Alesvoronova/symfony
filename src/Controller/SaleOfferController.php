<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\SaleOfferType;
use App\Model\SaleOffer\UseCase\CreateSaleOfferDtoTest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sale-offer", name="sale_offer")
 */
class SaleOfferController extends AbstractController
{
    /**
     * @Route("/create", name=".create")
     */
    public function create(Request $request)
    {
        $cmd = new CreateSaleOfferDtoTest();
        $form = $this->createForm(SaleOfferType::class, $cmd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //todo
        }
        $form->createView();

        return $this->render('app/sale-offer/create.html.twig', [
            'title' => 'Офрмление заявки',
            'form' => $form->createView(),
        ]);
    }
}
