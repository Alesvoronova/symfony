<?php

namespace App\ReadModel\SaleOffer;

use Doctrine\DBAL\Connection;

class SaleOfferFetcher
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function exist(string $str)
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT (*)')
            ->from('sale_offers')
            ->where('reset_token = :token')
            ->setParameter(':token', $str)
            ->execute()->fetchColumn(0) > 0;
    }
}
