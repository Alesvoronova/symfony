<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Tariff;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tariff|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tariff|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tariff[]    findAll()
 * @method Tariff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TariffRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tariff::class);
    }

    public function findAllActiveTariffs(): array
    {
        $qb = $this->createQueryBuilder('t');
        $expr = $qb->expr();

        return $qb
            ->andWhere($expr->lte('t.createdAt', ':createdAt'))
            ->andWhere($expr->gte('t.expiresAt', ':expiresAt'))
            ->setParameters([
                'createdAt' => new DateTime(),
                'expiresAt' => new DateTime(),
            ])
            ->getQuery()
            ->getResult();
    }
}
