<?php

declare(strict_types=1);

namespace App\DataFixtures\Appeal;

use App\Model\Appeal\Entity\Appeal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppealFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $appeal1 = new Appeal(
            $faker->uuid,
            $faker->name,
            $faker->email,
            $faker->text,
            $faker->text,
            new \DateTimeImmutable($faker->dateTime()->format('Y-m-d'))
        );
        $manager->persist($appeal1);
        $appeal2 = new Appeal(
            $faker->uuid,
            $faker->name,
            $faker->email,
            $faker->text,
            $faker->text,
            new \DateTimeImmutable($faker->dateTime()->format('Y-m-d'))
        );
        $manager->persist($appeal2);
        $appeal3 = new Appeal(
            $faker->uuid,
            $faker->name,
            $faker->email,
            $faker->text,
            $faker->text,
            new \DateTimeImmutable($faker->dateTime()->format('Y-m-d'))
        );
        $manager->persist($appeal3);

        $manager->flush();
    }
}
