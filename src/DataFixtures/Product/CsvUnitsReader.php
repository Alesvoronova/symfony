<?php

declare(strict_types=1);

namespace App\DataFixtures\Product;

use App\Model\Product\Entity\Unit;

class CsvUnitsReader
{
    private function parse(): array
    {
        $rows = array_map(
            'str_getcsv',
            file(__DIR__ . '/units.csv')
        );
        $header = array_shift($rows);
        $csv = [];
        foreach ($rows as $row) {
            $csv[] = array_combine($header, $row);
        }

        return $csv;
    }

    public function handle(): array
    {
        $data = $this->parse();
        $units = [];

        foreach ($data as $unit) {
            $units[] = new Unit(
                $unit['NAME_CODE'],
                $unit['CODE'],
                $unit['SYMBOL'],
            );
        }

        return $units;
    }
}
