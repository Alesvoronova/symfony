<?php

declare(strict_types=1);

namespace App\DataFixtures\Product;

use App\Model\Product\UseCase\Create\ProductCreateHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    private ProductCreateHandler $handler;

    public function __construct(ProductCreateHandler $handler)
    {
        $this->handler = $handler;
    }

    public function load(ObjectManager $manager): void
    {
        $prod1 = $this->handler->handle(
            '11111',
            'Название 1',
            'Описание 1',
            '4'
        );
        $manager->persist($prod1);

        $prod2 = $this->handler->handle(
            '22222',
            'Название 2',
            'Описание 2',
            '3'
        );
        $manager->persist($prod2);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [UnitFixtures::class];
    }
}
