<?php

declare(strict_types=1);

namespace App\DataFixtures\Product;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UnitFixtures extends Fixture
{
    private CsvUnitsReader $reader;

    public function __construct(CsvUnitsReader $reader)
    {
        $this->reader = $reader;
    }

    public function load(ObjectManager $manager)
    {
        $units = $this->reader->handle();
        if (is_array($units)) {
            foreach ($units as $unit) {
                $manager->persist($unit);
            }
        }
        $manager->flush();
    }
}
