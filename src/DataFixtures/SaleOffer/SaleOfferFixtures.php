<?php

declare(strict_types=1);

namespace App\DataFixtures\SaleOffer;

use App\DataFixtures\Product\ProductFixtures;
use App\DataFixtures\User\CounterpartyFixture;
use App\DataFixtures\User\UserFixtures;
use App\Model\Product\Entity\Product;
use App\Model\Product\Repository\ProductRepository;
use App\Model\SaleOffer\Entity\SaleOffer;
use App\Model\User\Entity\Counterparty;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class SaleOfferFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var ProductRepository
     */
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function load(ObjectManager $manager): void
    {
        $this->createMany($manager);
    }

    public function createMany(ObjectManager $manager): void
    {
//        $productRepository = $manager->getRepository(Product::class);
        $counterpartyRepository = $manager->getRepository(Counterparty::class);
        /** @var Product[] $products */
        $products = $this->repository->findAll();
        /** @var Counterparty[] $counterparties */
        $counterparties = $counterpartyRepository->findAll();

        $faker = Factory::create();
        $count = count($products) - 1;
        for ($i = 0; $i < $count; $i++) {
            $entity = $this->create($faker, $products[$i], $faker->randomElement($counterparties));
            $manager->persist($entity);
        }

        $manager->flush();
    }

    private function create(
        Generator $faker,
        Product $product,
        Counterparty $counterparty
    ): SaleOffer {
        return new SaleOffer(
            $faker->uuid,
            $product,
            $counterparty,
            new \DateTimeImmutable($faker->dateTime()->format('Y-m-d')),
            $faker->text,
            $faker->numberBetween(0, 1000),
            1
        );
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            ProductFixtures::class,
            CounterpartyFixture::class,
        ];
    }
}
