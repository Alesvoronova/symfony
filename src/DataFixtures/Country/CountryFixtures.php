<?php

declare(strict_types=1);

namespace App\DataFixtures\Country;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new Country('Беларусь'));
        $manager->persist(new Country('Россия'));

        $manager->flush();
    }
}
