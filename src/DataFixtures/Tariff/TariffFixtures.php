<?php

namespace App\DataFixtures\Tariff;

use App\Entity\Tariff;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class TariffFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
   //it are active tariffs
        $tariff1 = new Tariff(
            Uuid::uuid4(),
            'Тестовый тариф 1',
            200.00,
            new \DateTime(),
            \DateTime::createFromFormat('Y-m-d', '2021-02-15')
        );
        $manager->persist($tariff1);

        $tariff2 = new Tariff(
            Uuid::uuid4(),
            'Тестовый тариф 2',
            250.00,
            new \DateTime(),
            \DateTime::createFromFormat('Y-m-d', '2020-11-15')
        );
        $manager->persist($tariff2);
        //its not active tariff
        $tariff3 = new Tariff(
            Uuid::uuid4(),
            'Тестовый тариф 3 (не активный)',
            250.00,
            \DateTime::createFromFormat('Y-m-d', '2020-02-15'),
            \DateTime::createFromFormat('Y-m-d', '2020-04-15')
        );
        $manager->persist($tariff3);

        $manager->flush();
    }
}
