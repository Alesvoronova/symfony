<?php

declare(strict_types=1);

namespace App\DataFixtures\User;

use App\Entity\City;
use App\Entity\Country;
use App\Model\User\Entity\Counterparty;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Ramsey\Uuid\Uuid;

class CounterpartyFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $country = new Country('Казахстан');
        $counterparty = new Counterparty(
            Uuid::uuid4(),
            $faker->name,
            $faker->numberBetween(0, 10000),
            $faker->numberBetween(0, 10000),
            $faker->numberBetween(0, 10000),
            $faker->phoneNumber,
            $faker->firstName,
            $faker->firstName,
            $faker->lastName,
            new City($faker->city, $country),
            $country,
            $faker->email
        );

        $manager->persist($counterparty);
        $manager->flush($counterparty);
    }
}
