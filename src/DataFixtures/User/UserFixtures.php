<?php

declare(strict_types=1);

namespace App\DataFixtures\User;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $adminUser = new User(Uuid::uuid4());
        $adminUser
            ->setEmail('admin@test.ru')
            ->setPassword($this->encoder->encodePassword($adminUser, '1234'))
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($adminUser);

        $seller = new User(Uuid::uuid4());
        $seller
            ->setEmail('seller@test.ru')
            ->setPassword($this->encoder->encodePassword($seller, '1234'))
            ->setRoles(['ROLE_SELLER']);
        $manager->persist($seller);

        $buyer = new User(Uuid::uuid4());
        $buyer
            ->setEmail('buyer@test.ru')
            ->setPassword($this->encoder->encodePassword($buyer, '1234'))
            ->setRoles(['ROLE_BUYER']);
        $manager->persist($buyer);

        $manager->flush();
    }
}
