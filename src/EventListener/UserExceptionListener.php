<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Exception\AppExceptionInterface;
use App\Kernel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Routing\RouterInterface;

class UserExceptionListener
{
    private SessionInterface $session;

    private RouterInterface $router;

    private Kernel $kernel;

    public function __construct(SessionInterface $session, RouterInterface $router, Kernel $kernel)
    {
        $this->session = $session;
        $this->router = $router;
        $this->kernel = $kernel;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        if ($exception instanceof AppExceptionInterface) {
            $this->session->getFlashBag()->add('error', sprintf('Ошибка: %s', $exception->getMessage()));
        } else {
            if ('prod' === $this->kernel->getEnvironment()) {
                $this->session->getFlashBag()->add('error', 'Что-то пошло не так, попробуйте еще раз');
            } else {
                return;
            }
        }

        $route = 'home';
        if ($route === $event->getRequest()->get('_route')) {
            return;
        }

        $url = $this->router->generate($route);
        $response = new RedirectResponse($url);
        $event->setResponse($response);
    }
}
