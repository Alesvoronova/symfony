<?php

declare(strict_types=1);

namespace App\Exception;

class BaseUserException extends \Exception implements AppExceptionInterface
{
}
