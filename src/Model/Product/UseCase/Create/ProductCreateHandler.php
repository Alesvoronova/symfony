<?php

declare(strict_types=1);

namespace App\Model\Product\UseCase\Create;

use App\Model\Product\Entity\Product;
use App\Model\Product\Repository\UnitRepository;
use Ramsey\Uuid\Uuid;

class ProductCreateHandler
{
    private UnitRepository $unitRepository;

    public function __construct(
        UnitRepository $unitRepository
    ) {
        $this->unitRepository = $unitRepository;
    }

    public function handle(string $barcode, string $name, string $description, string $unitCode): Product
    {
        return new Product(
            Uuid::uuid4(),
            $barcode,
            $name,
            $description,
            $this->unitRepository->getByCode($unitCode)
        );
    }
}
