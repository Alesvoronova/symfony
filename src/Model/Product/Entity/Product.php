<?php

declare(strict_types=1);

namespace App\Model\Product\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use App\Model\Product\Repository\ProductRepository;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $barcode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $description;

    /**
     * @ORM\ManyToOne(targetEntity="Unit", inversedBy="products")
     * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     */
    private Unit $unit;

    public function __construct(
        UuidInterface $id,
        string $barcode,
        string $name,
        string $description,
        Unit $unit
    ) {
        $this->id = $id;
        $this->barcode = $barcode;
        $this->name = $name;
        $this->description = $description;
        $this->unit = $unit;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
