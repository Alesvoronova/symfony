<?php

declare(strict_types=1);

namespace App\Model\Product\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Model\Product\Repository\UnitRepository;

/**
 * @ORM\Entity(repositoryClass=UnitRepository::class)
 * @ORM\Table(name="product_units")
 */
class Unit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $symbol;

    public function __construct(string $name, string $code, string $symbol)
    {
        $this->name = $name;
        $this->code = $code;
        $this->symbol = $symbol;
    }

    public function __toString(): string
    {
        return $this->symbol;
    }
}
