<?php

declare(strict_types=1);

namespace App\Model\SaleOffer\UseCase;

use App\Model\SaleOffer\Entity\SaleOffer;
use App\Model\Product\Repository\ProductRepository;
use App\Model\User\Repository\CounterpartyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class CreateSaleOfferUC
{
    private EntityManagerInterface $em;
    private ProductRepository $productRepository;
    private CounterpartyRepository $counterpartyRepository;

    public function __construct(
        EntityManagerInterface $em,
        ProductRepository $productRepository,
        CounterpartyRepository $counterpartyRepository
    ) {
        $this->em = $em;
        $this->productRepository = $productRepository;
        $this->counterpartyRepository = $counterpartyRepository;
    }

    public function handle(CreateSaleOfferDto $dto): SaleOffer
    {
        $product = $this->productRepository->getById($dto->product);
        $counterparty = $this->counterpartyRepository->getById($dto->counterparty);

        $saleOffer = new SaleOffer(
            Uuid::uuid4(),
            $product,
            $counterparty,
            new \DateTimeImmutable(),
            $dto->comment,
            $dto->price,
            $dto->currency,
            $dto->count
        );

        $this->em->persist($saleOffer);
        $this->em->flush();

        return $saleOffer;
    }
}
