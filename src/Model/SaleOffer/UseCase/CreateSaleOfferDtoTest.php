<?php

declare(strict_types=1);

namespace App\Model\SaleOffer\UseCase;

class CreateSaleOfferDtoTest
{
    public string $barcode;
    public string $author;
    public int $count;
    public int $unit;
    public int $price;
    public string $comment;
}
