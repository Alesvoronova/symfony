<?php

declare(strict_types=1);

namespace App\Model\SaleOffer\UseCase;

use Symfony\Component\Validator\Constraints as Assert;

class CreateSaleOfferDto
{
    /**
     * @Assert\NotBlank()
     */
    public int $article;
    /**
     * @Assert\NotBlank()
     */
    public string $product;
    public string $counterparty;
    public int $count;
    public int $price;
    public int $currency;
    public bool $isAgreed;
    public string $comment;
}
