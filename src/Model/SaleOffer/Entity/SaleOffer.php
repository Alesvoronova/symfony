<?php

namespace App\Model\SaleOffer\Entity;

use App\Model\Product\Entity\Product;
use App\Model\User\Entity\Counterparty;
use App\Repository\SaleOfferRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaleOfferRepository::class)
 * @ORM\Table(name="sale_offers", uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"counterparty_id", "product_id"})
 * })
 */
class SaleOffer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\Product\Entity\Product", inversedBy="sale_offers")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private Product $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Model\User\Entity\Counterparty", inversedBy="sale_offers")
     * @ORM\JoinColumn(name="counterparty_id", referencedColumnName="id", nullable=false, onDelete="cascade")
     */
    private Counterparty $counterparty;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private DateTimeImmutable $updatedAt;

    /**
     * @ORM\Column(type="text")
     */
    private string $comment;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $price;

    /**
     * @ORM\Column(type="integer")
     */
    private int $count;

    /**
     * @ORM\Column(type="integer")
     */
    private int $views;

    /**
     * @ORM\Column(name="expires", type="datetime_immutable")
     */
    private DateTimeImmutable $expires;

    /**
     * @ORM\Column(type="integer")
     */
    private int $currency;

    public function __construct(
        $id,
        Product $product,
        Counterparty $counterparty,
        DateTimeImmutable $createdAt,
        string $comment,
        ?int $price,
        int $currency
    ) {
        $this->id = $id;
        $this->product = $product;
        $this->counterparty = $counterparty;
        $this->createdAt = $createdAt;
        $this->comment = $comment;
        $this->price = $price;
        $this->count = 0;
        $this->currency = $currency;
        $this->views = 0;
        $this->expires = new DateTimeImmutable();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getViews(): int
    {
        return $this->views;
    }

    public function getCounterparty(): Counterparty
    {
        return $this->counterparty;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }
}
