<?php

declare(strict_types=1);

namespace App\Model\Appeal\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="appleas")
 */
class Appeal
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $fio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $subject;

    /**
     * @ORM\Column(type="text")
     */
    private string $description;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $updatedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private string $answer;

    public function __construct($id, $fio, $email, $subject, $description, $createdAt)
    {
        $this->id = $id;
        $this->fio = $fio;
        $this->email = $email;
        $this->subject = $subject;
        $this->description = $description;
        $this->createdAt = $createdAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFio(): string
    {
        return $this->fio;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function isRead(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function addAnswer(string $message): void
    {
        $this->answer = $message;
        $this->updatedAt = new \DateTimeImmutable();
    }
}
