<?php

declare(strict_types=1);

namespace App\Model\Appeal\Send;

use Symfony\Component\Validator\Constraints as Assert;

class AppealCommand
{
    /**
     * @Assert\NotBlank()
     */
    public string $fio;
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public string $email;
    /**
     * @Assert\NotBlank()
     */
    public string $subject;
    /**
     * @Assert\NotBlank()
     */
    public string $description;
}
