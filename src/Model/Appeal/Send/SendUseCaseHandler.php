<?php

declare(strict_types=1);

namespace App\Model\Appeal\Send;

use App\Model\Appeal\Entity\Appeal;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class SendUseCaseHandler
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(AppealCommand $command)
    {
        $appeal = new Appeal(
            Uuid::uuid4(),
            $command->fio,
            $command->email,
            $command->subject,
            $command->description,
            new \DateTimeImmutable()
        );

        $this->em->persist($appeal);
        $this->em->flush();
    }
}
