<?php

declare(strict_types=1);

namespace App\Model\Appeal\UseCase\Answer;

use Doctrine\ORM\EntityManagerInterface;

class AnswerUC
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handle(AppealAnswerDto $dto)
    {
        $appeal = $dto->appeal;
        $appeal->addAnswer($dto->answer);
        $this->em->persist($appeal);
        $this->em->flush();
    }
}
