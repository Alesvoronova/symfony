<?php

declare(strict_types=1);

namespace App\Model\Appeal\UseCase\Answer;

use App\Model\Appeal\Entity\Appeal;
use Symfony\Component\Validator\Constraints as Assert;

class AppealAnswerDto
{
    /**
     * @Assert\NotBlank
     */
    public string $answer;

    public Appeal $appeal;
}
