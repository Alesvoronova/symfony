<?php

declare(strict_types=1);

namespace App\Model\User\Repository;

use App\Model\User\Entity\ConnectionRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConnectionRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConnectionRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConnectionRequest[]    findAll()
 * @method ConnectionRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConnectionRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConnectionRequest::class);
    }

    public function findAllWithSorting(): array
    {
        $qb = $this->createQueryBuilder('cr');

        return $qb
            ->leftJoin('cr.counterparty', 'cp')
            ->leftJoin('cr.tariff', 't')
            ->addSelect('t')
            ->addSelect('cp')
            ->orderBy('cr.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
