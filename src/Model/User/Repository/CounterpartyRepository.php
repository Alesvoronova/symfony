<?php

declare(strict_types=1);

namespace App\Model\User\Repository;

use App\Model\User\Entity\Counterparty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Counterparty|null find($id, $lockMode = null, $lockVersion = null)
 * @method Counterparty|null findOneBy(array $criteria, array $orderBy = null)
 * @method Counterparty[]    findAll()
 * @method Counterparty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterpartyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Counterparty::class);
    }

    public function getById($id): Counterparty
    {
        $product = $this->findOneBy($id);
        if (!$product) {
            throw new \DomainException('Counterparty was not found');
        }

        return $product;
    }
}
