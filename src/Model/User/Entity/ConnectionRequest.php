<?php

declare(strict_types=1);

namespace App\Model\User\Entity;

use App\Model\User\Repository\ConnectionRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;
use App\Entity\Tariff;

/**
 * @ORM\Entity(repositoryClass=ConnectionRequestRepository::class)
 */
class ConnectionRequest
{
    public const CONNECTION_REQUEST_TYPE = 'Заявка на подключение';

    public const RENEWAL_REQUEST_TYPE = 'Заявка на продление';

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\ManyToOne(targetEntity=Tariff::class, cascade={"persist"})
     * @ORM\JoinColumn(name="tariff_id", referencedColumnName="id")
     */
    private Tariff $tariff;

    /**
     * @ORM\ManyToOne(targetEntity=Counterparty::class, cascade={"persist"})
     * @ORM\JoinColumn(name="counterparty_id", referencedColumnName="id")
     */
    private Counterparty $counterparty;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isApprove;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @ORM\Column(type="string")
     */
    private string $type;

    public function __construct(
        UuidInterface $id,
        Tariff $tariff,
        Counterparty
        $counterparty,
        bool $isApprove,
        \DateTime $createdAt,
        string $type
    ) {
        $this->id = $id;
        $this->tariff = $tariff;
        $this->counterparty = $counterparty;
        $this->isApprove = $isApprove;
        $this->createdAt = $createdAt;
        $this->type = $type;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getTariff(): ?Tariff
    {
        return $this->tariff;
    }

    public function getCounterparty(): ?Counterparty
    {
        return $this->counterparty;
    }

    public function isApprove(): ?bool
    {
        return $this->isApprove;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function approve(): void
    {
        $this->isApprove = true;
    }
}
