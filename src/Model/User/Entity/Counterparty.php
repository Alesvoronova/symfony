<?php

declare(strict_types=1);

namespace App\Model\User\Entity;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\User;
use App\Model\User\Repository\CounterpartyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass=CounterpartyRepository::class)
 * @ORM\Table(name="`counterparties`",
 *     uniqueConstraints={
 *        @UniqueConstraint(name="company_unique",
 *            columns={"ynp", "country_id"})
 *    })
 */
class Counterparty
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $companyName;

    /**
     * @ORM\Column(type="integer")
     */
    private int $ynp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $kpp;

    /**
     * @ORM\Column(type="integer")
     */
    private int $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $contactFirstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $contactMiddleName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $contactLastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $email;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, cascade={"persist"})
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private City $city;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, cascade={"persist"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private Country $country;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="counterparty", cascade={"persist"})
     */
    private $users;

    public function __construct(
        UuidInterface $id,
        string $companyName,
        int $ynp,
        ?int $kpp,
        int $zipCode,
        string $phone,
        string $contactFirstName,
        string $contactMiddleName,
        string $contactLastName,
        City $city,
        Country $country,
        string $email
    ) {
        $this->id = $id;
        $this->companyName = $companyName;
        $this->ynp = $ynp;
        $this->kpp = $kpp;
        $this->zipCode = $zipCode;
        $this->phone = $phone;
        $this->contactFirstName = $contactFirstName;
        $this->contactMiddleName = $contactMiddleName;
        $this->contactLastName = $contactLastName;
        $this->city = $city;
        $this->country = $country;
        $this->email = $email;
    }

    public function add(User $user)
    {
        $this->users->add($user->getId());
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    public function getYnp(): int
    {
        return $this->ynp;
    }

    public function getKpp(): ?int
    {
        return $this->kpp;
    }

    public function getZipCode(): int
    {
        return $this->zipCode;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getContactFirstName(): string
    {
        return $this->contactFirstName;
    }

    public function getContactMiddleName(): string
    {
        return $this->contactMiddleName;
    }

    public function getContactLastName(): string
    {
        return $this->contactLastName;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getUsers(): PersistentCollection
    {
        return $this->users;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }
}
