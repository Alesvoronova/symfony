<?php

declare(strict_types=1);

namespace App\Model\User\UseCase;

use App\Entity\City;
use App\Entity\User;
use App\Exception\BaseUserException;
use App\Model\User\Entity\ConnectionRequest;
use App\Model\User\Entity\Counterparty;
use App\Repository\CityRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class ConnectionRequestHandler
{
    private CityRepository $repository;
    private EntityManagerInterface $em;

    public function __construct(CityRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    public function handle(ConnectionRequestCommand $command, User $user): void
    {
        $city = $this->repository->findCityInCountry($command->country);
        try {
            $counterparty = new Counterparty(
                Uuid::uuid4(),
                $command->companyName,
                $command->ynp,
                (int) $command->kpp,
                $command->zipCode,
                $command->phone,
                $command->contactFirstName,
                $command->contactMiddleName,
                $command->contactLastName,
                $city === null ? new City($command->city, $command->country) : $city,
                $command->country,
                $command->email,
            );
            $this->em->persist($counterparty);

            $connectionRequest = new ConnectionRequest(
                Uuid::uuid4(),
                $command->tariff,
                $counterparty,
                false,
                new \DateTime(),
                ConnectionRequest::CONNECTION_REQUEST_TYPE
            );
            $this->em->persist($connectionRequest);
            $user->assignCounterparty($counterparty);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new BaseUserException('Контрагент уже добавлен. Обратитесь в техподдержку');
        }
    }
}
