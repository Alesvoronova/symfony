<?php

declare(strict_types=1);

namespace App\Model\User\UseCase;

use App\Entity\Country;
use App\Entity\Tariff;
use Symfony\Component\Validator\Constraints as Assert;

class ConnectionRequestCommand
{
    /**
     * @Assert\NotBlank
     */
    public string $companyName;

    /**
     *@Assert\NotBlank
     */
    public string $city;

    /**
     *@Assert\NotBlank
     */
    public int $ynp;

    /**
     * @Assert\NotBlank
     */
    public Country $country;

    /**
     *@Assert\NotBlank
     */
    public int $zipCode;

    /**
     *@Assert\NotBlank
     */
    public string $address;

    /**
     *@Assert\NotBlank
     */
    public string $phone;

    /**
     *@Assert\NotBlank
     */
    public string $contactFirstName;

    public string $contactMiddleName;
    /**
     *@Assert\NotBlank
     */
    public string $contactLastName;

    /**
     *@Assert\NotBlank
     */
    public Tariff $tariff;

    public string $kpp;
    /**
     *@Assert\NotBlank
     */
    public string $email;
}
