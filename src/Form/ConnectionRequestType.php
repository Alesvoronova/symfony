<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Country;
use App\Entity\Tariff;
use App\Model\User\UseCase\ConnectionRequestCommand;
use App\Repository\TariffRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConnectionRequestType extends AbstractType
{
    private TariffRepository $tariff;

    public function __construct(TariffRepository $tariff)
    {
        $this->tariff = $tariff;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, [
                'label' => 'Название организации'
            ])
            ->add('city', TextType::class, [
                'label' => 'Город'
            ])
            ->add('ynp', IntegerType::class, [
                'label' => 'ИНН(УНП)'
            ])
            ->add('country', EntityType::class, [
                'label' => 'Страна',
                'class' => Country::class
            ])
            ->add('zipCode', IntegerType::class, [
                'label' => 'Индекс'
            ])
            ->add('address', TextType::class, [
                'label' => 'Юридический адрес',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Телефон (факс)',
            ])
            ->add('contactFirstName', TextType::class, [
                'label' => 'Имя',
            ])
            ->add('contactMiddleName', TextType::class, [
                'label' => 'Отчество',
                'attr' => [
                    'required' => false,
                ],
            ])
            ->add('contactLastName', TextType::class, [
                'label' => 'Фамилия'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email'
            ])
            ->add('tariff', EntityType::class, [
                'class' => Tariff::class,
                'label' => 'Тарифный план',
                'choices' => $this->tariff->findAllActiveTariffs()
            ])
            ->add('kpp', IntegerType::class, [
                'label' => 'КПП',
                'attr' => [
                    'required' => false
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Оформить'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ConnectionRequestCommand::class
        ]);
    }
}
