<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SaleOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class, ['label' => 'Автор'])
            ->add('barcode', SearchType::class, ['label' => 'Артикул'])
//            ->add('count', TextType::class, ['label' => 'Количество'])
//            ->add('price', TextType::class, ['label' => 'Цена'])
//            ->add('currencies', TextType::class, ['label' => 'Цена'])
            ->add('price')
            ->add('comment')
            ->add('price')
        ;
    }
}
