<?php

declare(strict_types=1);

namespace App\Form;

use App\Model\Appeal\Send\AppealCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AppealType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fio', TextType::class, ['label' => 'Имя и фамилия'])
            ->add('email', EmailType::class, ['label' => 'Электронная почта'])
            ->add('subject', TextType::class, ['label' => 'Тема обращения'])
            ->add('description', TextareaType::class, ['label' => 'Обращение']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AppealCommand::class,
        ]);
    }
}
