<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\TariffRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=TariffRepository::class)
 * @ORM\Table(name="`tariffs`")
 */
class Tariff
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private UuidInterface $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\Column(type="float")
     */
    private float $cost;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTimeInterface $expiresAt;

    /**
     * Tariff constructor.
     * @param UuidInterface $id
     * @param string $name
     * @param float $cost
     * @param \DateTimeInterface $createdAt
     * @param \DateTimeInterface $expiresAt
     */
    public function __construct(
        UuidInterface $id,
        string $name,
        float $cost,
        \DateTimeInterface $createdAt,
        \DateTimeInterface $expiresAt
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->cost = $cost;
        $this->createdAt = $createdAt;
        $this->expiresAt = $expiresAt;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCost(): float
    {
        return $this->cost;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getExpiresAt(): \DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}
