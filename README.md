# Stock Exchange (Биржа)

## Инструменты для разработки:

### Проверить код стайл:
- php vendor/bin/phpcs

### Анализ кода:
https://phpstan.org/user-guide/getting-started
- php vendor/bin/phpcs

### Тесты
запустить тесты можно выполнив:
- symfony run bin/phpunit
- php bin/phpunit


docker-compose run --rm stock-node yarn install
docker-compose run --rm stock-node npm run dev
docker-compose run --rm stock-node npm run watch
